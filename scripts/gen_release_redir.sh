#!/usr/bin/env bash

RELEASEJSON="release.json"


function html_output( ) {
  URL=$1

  cat <<EOF

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Refresh" content="7; url=$URL" />
  </head>
  <body>
    <p>Please follow <a href="$URL">this link</a>.</p>
  </body>
</html>

EOF


}

curl --header "PRIVATE-TOKEN: $RELEASEAPIKEY"  $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases > $RELEASEJSON

echo using release
cat $RELEASEJSON |  jq -e -r '. | sort_by(.released_at)[-1]'

if [ $? -ne 0 ]; then
	echo jq failed to extract 'released_at'
	cat $RELEASEJSON
	exit 1
fi

UCLTMPLURL=$(cat $RELEASEJSON |  jq -e -r '. | sort_by(.released_at)[-1].assets.links[].direct_asset_url | select( contains( "ucl_template" ) )')
if [ $? -ne 0 ]; then
	echo jq failed to extract 'ucl_template'
	cat $RELEASEJSON
	exit 1
fi
echo "ucl_template url $UCLTMPLURL"
echo ""
html_output $UCLTMPLURL > public/ucl_template-latest.tgz
echo $UCLTMPLURL > ucl_template.url

SCRIPTSURL=$(cat $RELEASEJSON |  jq -e -r '. | sort_by(.released_at)[-1].assets.links[].direct_asset_url | select( contains( "doc_scripts" ) )')
if [ $? -ne 0 ]; then
	echo jq failed to extract 'doc_scripts'
	cat $RELEASEJSON
	exit 1
fi
echo "doc_scripts url $SCRIPTSURL"
echo ""
html_output $SCRIPTSURL > public/doc_scripts-latest.tgz
echo $SCRIPTSURL > doc_scripts.url
